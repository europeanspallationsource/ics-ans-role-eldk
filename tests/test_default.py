import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_default(File):
    assert File('/export/sdk/eldk-5.6/ifc1210/sysroots').is_directory


def test_opt_eldk_link(File):
    assert File('/opt/eldk-5.6').linked_to == '/export/sdk/eldk-5.6'
