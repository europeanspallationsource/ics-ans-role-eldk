ics-ans-role-eldk
=================

Ansible role to install ELDK filesystem.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
eldk_root: /export/sdk
eldk_version: 5.6
eldk_archive: https://artifactory.esss.lu.se/artifactory/ansible-provision/eldk-{{ eldk_version }}.tar.gz
```

The `eldk_archive` is unarchived under the `eldk_root` directory.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-eldk
```

License
-------

BSD 2-clause
